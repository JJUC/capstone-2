let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	// Get the value of #courseName and assign it to the courseName variable
	let courseName = document.querySelector("#courseName").value
	// Get the value of #courseDescrpition and assign it to the description variable
	let description = document.querySelector("#courseDescription").value
	// Get thee value of thee #price and assign it to the eprice variable
	let price = document.querySelector("#coursePrice").value
	// retrieve the JSON web token stored in our local storage for authentication
	let token = localStorage.getItem('token')

	fetch('http://localhost:3000/api/courses',{
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then (res => {
		return res.json()
	})
	.then (data => {
		if(data === true){
			window.location.replace('./courses.html')
		}else{
			alert('Something went wrong')
		}
	})
})