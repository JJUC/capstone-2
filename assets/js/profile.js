let adminUser = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")
let profileContainer = document.getElementById("profileContainer")
let courseData;

let courseIds = []
let courseNames = []


fetch('http://localhost:3000/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(user => {

	for(let i = 0; i < user.enrollments.length; i++){
		courseIds.push(user.enrollments[i].courseId)
	}

	fetch('http://localhost:3000/api/courses')
	.then(res => res.json())
	.then(data => {

		for(let i = 0; i < data.length; i++){

			if(courseIds.includes(data[i]._id)){
				courseNames.push(data[i].name)
			}
	}

let coursesList = courseNames.join(`<br>`)


if(adminUser == "false" || !adminUser){
let profileName = document.getElementById("profileName")
let profileEmail = document.getElementById("profileEmail")
let profileMobile = document.getElementById("profileMobile")
let profileList = document.getElementById("profileList")

profileName.innerHTML = "Full Name:" + " " + user.firstName + " " + user.lastName
profileEmail.innerHTML = "Email Address: " + user.email
profileMobile.innerHTML = "Contact Number: " + user.mobileNo
profileList.innerHTML =  "Enrolled Courses: " + coursesList

console.log(adminUser)

}else{
let profileName = document.getElementById("profileName")
let profileEmail = document.getElementById("profileEmail")
let profileMobile = document.getElementById("profileMobile")
let profileList = document.getElementById("profileList")


profileName.innerHTML = "Full Name:" + " " + user.firstName + " " + user.lastName
profileEmail.innerHTML = "Email Address: " + user.email
profileMobile.innerHTML = "Contact Number: " + user.mobileNo
adminStatus.innerHTML =  adminUser
}



 
})
})
