// instantiate a URLSearchParams object so we can execute methods to access 
// specific parts of the query string
let params = new URLSearchParams(window.location.search);

// get method returns the value of the key passed in as an argument

let courseId = params.get('courseId')
let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")
let token = localStorage.getItem('token')

document.querySelector("#deleteCourse").addEventListener("click", (e) => {
        e.preventDefault()
        let token = localStorage.getItem('token')
        fetch(`http://localhost:3000/api/courses/${courseId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
 }).then(res => {
            return res.json()
        }).then(data => {
            //deleteion of course successful
            if(data === true){
                //redirect to courses index page
                window.location.replace("./courses.html")
            }else{
                //error in deletion course, redirect to error page
                alert("Something went wrong")
            }
        })
        })