let loginForm = document.querySelector("#logInUser")
let message =  document.querySelector("#message")
let navItems =  document.querySelector("#navSession")

let userToken = localStorage.getItem("token")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	if(email === "" || password === ""){
		alert("Please input your email and/or password")
	}else{
		fetch('http://localhost:3000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data.access){
				//set JWT in local storagee
				localStorage.setItem('token', data.access)
				fetch ('http://localhost:3000/api/users/details',{
					headers: {
						Authorization: `Bearer ${data.access}`
					}	
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					//set the global user state to have properties containing the authenticated user's ID and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")
				})
			}else
				alert("Something Went Wrong")
		})
	}
})


if(!userToken){
	navItems.innerHTML = 
	`
	<li class="nav-item">
		<a href="./register.html" class="nav-link">Register</a>
	</li>
	`
}else{
	navItems.innerHTML = `
	<ul class="navbar-nav nav-flex-icons">
        <li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
		<li class="nav-item">
			<a href="./logout.html" class="nav-link">Log out</a>
		</li>
          <span id="navSession"></span>
        </ul>
	`		
}

