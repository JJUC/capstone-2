let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let cardFooter;

if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML = `<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-info">Add Course</a>
</div>`
}

//fetch the courses from our API
fetch('http://localhost:3000/api/courses')
.then(res => res.json())
.then(data => {
	// console.log(data)
	let courseData;

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		courseData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block editButton">Select Course</a>`
			}else{
				if(course.isActive === true){
					//if user is an admin, display the edit course button
				cardFooter = 
				`
				<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block editButton">View Course</a>
				<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block editButton">Edit</a>
				<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block editButton">Delete</a>`

				}
				else{
				cardFooter = 
				`

				<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block editButton">View Course</a>
				<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block editButton">Edit</a>
				<a href="./activateCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block editButton">Activate</a>`
				}
			}
			return (
					`<div class="col-md-6 my-3">
	<div id="courseCard" class="card">
		<div class="card-body ">
			<h5 class="card-title">${course.name}</h5>
			<p class="card-text text-left">${course.description}</p>
			<hr>
			<p class="card-text text-right">Price: ₱${course.price}</p>
		</div>

		<div class="card-footer">
			${cardFooter}
		</div>
	</div>
</div>`
					)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}

	let container = document.querySelector('#coursesContainer')
	container.innerHTML = courseData
})

$('#recipeCarousel').carousel({
  interval: 10000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});
